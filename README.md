# Engineering Notes

This repo contains a collection of software engineering resources for the general public.

Resources available from the project wiki at https://gitlab.com/blambell/engineering-notes/-/wikis/home.

